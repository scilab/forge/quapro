// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// The following problem is extracted from 
// "Practical Optimization", Antoniou, Lu, 2007, 
// Chapter 11, "Linear Programming Part I: The simplex method", 
// Example 11.9, p. 361. 
// This problem is solved by the primal affine scaling algorithm 
// in Chapter 12, "Linear Programming Part II: Interior point methods", 
// Example 12.2, p.382.

A = [
 2 -2 -1
-1 -4  1
];
b = [-1;-1];
c = [2;9;3];
ci = [0;0;0];
cs = [%inf;%inf;%inf];
[xopt,lopt,fopt]=linpro(c,A,b,ci,cs);
xstar = [0 1/3 1/3]';
fstar = 4;
lstar  = [-8.5;0;0;3.5;0.5];
assert_checkalmostequal(xopt,xstar);
assert_checkalmostequal(fopt,fstar);
assert_checkalmostequal(lopt,lstar);

