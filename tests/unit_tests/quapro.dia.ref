// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
// The following problem is extracted from 
// "Practical Optimization", Antoniou, Lu, 2007, 
// Chapter 11, "Linear Programming Part I: The simplex method", 
// Example 11.9, p. 361. 
// This problem is solved by the primal affine scaling algorithm 
// in Chapter 12, "Linear Programming Part II: Interior point methods", 
// Example 12.2, p.382.
//Find x in R^6 such that:
//C1*x = b1 (3 equality constraints i.e me=3)
C1= [1,-1,1,0,3,1;
    -1,0,-3,-4,5,6;
     2,5,3,0,1,0];
b1=[1;2;3];
//C2*x <= b2 (2 inequality constraints)
C2=[0,1,0,1,2,-1;
    -1,0,2,1,1,0];
b2=[-1;2.5];
//with  x between ci and cs:
ci=[-1000;-10000;0;-1000;-1000;-1000];
cs=[10000;100;1.5;100;100;1000];
//and minimize 0.5*x'*Q*x + p'*x with
p=[1;2;3;4;5;6];
Q=eye(6,6);
//No initial point is given;
C=[C1;C2] ; //
b=[b1;b2] ;  //
me=3;
[xopt,lopt,fopt]=quapro(Q,p,C,b,ci,cs,me)
 fopt  = 
  -14.843248
 lopt  = 
   0.
   0.
   0.
   0.
   0.
   0.
  -1.5564027
  -0.1698164
  -0.7054782
   0.3091368
   0.
 xopt  = 
   1.7975426
  -0.3381487
   0.163388
  -4.9884023
   0.6054943
  -3.1155623
// From computed results:
 f_expected  = -1.484324778630451824D+01  ;
 l_expected  = [ 
    0.000000000000000000D+00  
    0.000000000000000000D+00  
    0.000000000000000000D+00  
    0.000000000000000000D+00  
    0.000000000000000000D+00  
    0.000000000000000000D+00  
  -1.556402664172172212D+00  
  -1.698163634189820970D-01  
  -7.054781513964311079D-01  
    3.091368166376714521D-01  
    0.000000000000000000D+00  
	];
 x_expected  = [ 
    1.797542603546051776D+00  
  - 3.381487238276857377D-01  
    1.633880281045186900D-01  
  - 4.988402270313599729D+00  
    6.054943277325134376D-01  
  - 3.115562338676264975D+00  
  ];
assert_checkalmostequal(fopt,f_expected);
assert_checkalmostequal(xopt,x_expected);
assert_checkalmostequal(lopt,l_expected);
//Only linear constraints (1 to 4) are active (lagr(1:6)=0):
[xopt,lopt,fopt]=quapro(Q,p,C,b,[],[],me);
   //Same result as above
 l_expected  = [ 
  -1.556402664172172212D+00  
  -1.698163634189820970D-01  
  -7.054781513964311079D-01  
    3.091368166376714521D-01  
    0.000000000000000000D+00  
	];
assert_checkalmostequal(fopt,f_expected);
assert_checkalmostequal(xopt,x_expected);
assert_checkalmostequal(lopt,l_expected);
