// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

path  = get_absolute_file_path("mps2linpro.tst");
filename = fullfile(path,"25FV47.SIF");
lp = mps2linpro(filename);
assert_checkequal(typeof(lp),"linpro");
assert_checkequal(size(lp.p),[1571,1]);
assert_checkequal(size(lp.C),[821,1571]);
assert_checkequal(size(lp.b),[821,1]);
assert_checkequal(size(lp.ci),[1571,1]);
assert_checkequal(size(lp.cs),[1571,1]);
assert_checkequal(lp.mi,516);
//
[p,C,b,ci,cs,mi] = mps2linpro(filename);
assert_checkequal(size(p),[1571,1]);
assert_checkequal(size(C),[821,1571]);
assert_checkequal(size(b),[821,1]);
assert_checkequal(size(ci),[1571,1]);
assert_checkequal(size(cs),[1571,1]);
assert_checkequal(mi,516);
// We could as well run :
// [xopt,lagr,fopt]=linpro(p,C,b,ci,cs);
// but this is another story, and certainly not the 
// purpose of this test.
