// Copyright (C) INRIA - Serge Steer
// Copyright (C) DIGITEO - 2010 - Allan CORNET
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
mode(-1);
demopath = get_absolute_file_path("quapro.dem.gateway.sce");

subdemolist = ["Optimal ressource allocation"  , "optloc/optloc.dem.sce"];

if grep(atomsGetInstalled(), "metanet") <> [] then
subdemolist = [subdemolist; ..
               "Multiflow problem" , "multiflow/multiflow.dem.sce"];
end

subdemolist(:,2) = demopath + subdemolist(:,2)
