// ====================================================================
// Copyright (C) INRIA -  Allan CORNET
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
help_lang_dir = get_absolute_file_path('build_help.sce');
xmlfiles=listfiles(help_lang_dir+'*.xml')
if newest([help_lang_dir+'date_build';xmlfiles])==1 then
  clear  xmlfiles help_lang_dir 
  return
end
try
  tbx_build_help(TOOLBOX_TITLE, help_lang_dir);
  mputl(sci2exp(getdate()),help_lang_dir+'date_build')
catch
 mprintf("%s\n",lasterror())
end

clear help_lang_dir sci2exp xmlfiles tbx_build_help

