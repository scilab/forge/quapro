Readme of the quapro module

Purpose
--------
This toolbox defines linear quadratic programming solvers.
The matrices defining the cost and constraints must be full, but the
quadratic term matrix is not required to be full rank.

Features
---------

 * linpro : linear programming solver
 * mps2linpro : convert lp problem given in MPS format to linpro format
 * quapro : linear quadratic programming solver

 The multiflow demo requires the metanet module.

Dependencies
------------

 * This module depends on the assert module.

Forge
------

http://forge.scilab.org/index.php/p/quapro/

ATOMS
-------

http://atoms.scilab.org/toolboxes/quapro

Authors
-------

 * Eduardo Casas Renteria and Cecilia Pola Mendez,
   Departamento de Matematicas, Estadistica y Computacion,
   Universidad de Cantabria, Spain (Solver code)
 * Eduardo Casas Renteria and Serge Steer, Metalau project,
   INRIA (interface with Scilab)
 * Francois Delebecque (multiflow demo)
 * Allan CORNET - DIGITEO - 2010 (update for Scilab 5.3)
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
