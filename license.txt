 +   The src/fortran/plcbas.f and it's subsidiary fortran files have the
     following Copyright:

      Eduardo Casas Renteria  and Cecilia Pola Mendez                           
      Departamento de Matematicas, Estadistica y Computacion         
                   UNIVERSIDAD DE CANTABRIA, Spain		                                                                

      Note that these files are not free. To use them out of Scilab please
      contact the authors.


 +    The files definining the interface with Scilab :

      macros/*.sci,  help/en_US/*.xml files have been developped by
      Eduardo Casas Renteria (Metalau project, INRIA)  and 
      Serge Steer (Metalau project, INRIA)

      demos:* files  have been developped by Serge Steer and Francois
      Delebecque (Metalau project, INRIA)

      builder and loader for Scilab-5.0 have been developped by Serge Steer
      and Allan Cornet (Digiteo)

      These files are released under the terms of the CeCILL.
      These files arelicensed as described in the file COPYING, which
      you should have received as part of this distribution.  The terms
      are also available at    
      http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
