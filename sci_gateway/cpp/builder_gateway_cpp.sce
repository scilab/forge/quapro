// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_cpp()

    gateway_path = get_absolute_file_path("builder_gateway_cpp.sce");
    libname = "quapro";
    namelist = [
        "quapro_internal" "sci_quapro_internal" "cppsci"
    ];
    files = [
        "sci_quapro_internal.cpp"
    ];
    ldflags = "";
    libs = ["../../src/fortran/libpclbas"];

    tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags);

endfunction

builder_gw_cpp();
clear builder_gw_cpp;
