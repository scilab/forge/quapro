//====================================================================
// Copyright (C) INRIA -  Eduardo Casas Renteria, Serge Steer
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http:cwww.cecill.info/licences/Licence_CeCILL_V2-en.txt
//====================================================================

#include "function.hxx"
#include "double.hxx"

extern "C" {
#include "Scierror.h"
#include "localization.h"
}

extern "C" {
void C2F(plcbas)(double* h,  double* p,  double* c,
                 double* d,  double* ci, double* cs,
                    int* ira,   int* mi,    int* md,
                 double* x,  double* f,  double* w,
                    int* iv, double* dlagr, int* imp,
                    int* io,    int* n,     int* modo,
                    int* info,  int* iter);
}

types::Function::ReturnValue
sci_quapro_internal(
        types::typed_list &in,
        int iRetCount,
        types::typed_list &out) {

    types::Double* o_x0   = NULL; // x0   as a scilab object
    types::Double* o_h    = NULL; // h    as a scilab object
    types::Double* o_p    = NULL; // p    as a scilab object
    types::Double* o_c    = NULL; // c    as a scilab object
    types::Double* o_d    = NULL; // d    as a scilab object
    types::Double* o_ci   = NULL; // ci   as a scilab object
    types::Double* o_cs   = NULL; // cs   as a scilab object
    types::Double* o_mi   = NULL; // mi   as a scilab object
    types::Double* o_modo = NULL; // modo as a scilab object
    types::Double* o_imp  = NULL; // imp  as a scilab object
    int n_x0 = 0;    // size of x0
    int n_h  = 0;    // num rows of h
    // int c_h = 0;    // num cols of h // unnecessary : h must be square
    int n_p  = 0;    // size of p
    int r_c  = 0;    // num rows of c
    int c_c  = 0;    // num cols of c
    int n_d  = 0;    // size of d
    int n_ci = 0;    // size of ci
    int n_cs = 0;    // size of cs
    int mi   = 0; // content of mi   (assuming Im(mi)   == 0.0)
    int modo = 0; // content of modo (assuming Im(modo) == 0.0)
    int imp  = 0; // content of imp  (assuming Im(imp)  == 0.0)
    int wte = 0; // input for plcbas
    int ira = 0; // Variable  que  indica  si  existen  restricciones  de
                 // acotacion en el problema. Toma los valores:
                 //     0  : No existen restricciones de acotacion.
                 //     1  : Se  tienen  solo  restricciones  de  acotacion
                 //          inferior.
                 //     2  : Existen solo restricciones de acotacion superior
                 //     3  : Se tienen ambos tipos de restriccion.
    int md  = 0; // TODO i have no clue what this is
    double* w  = NULL; // workspace vector
    int* iv = NULL; // workspace vector
    double* x0_cont = NULL; // temporary pointer to content of x0
    types::Double* x    = NULL; // output : optimal coordinates
    types::Double* f    = NULL; // output : optimal functionnal value
    types::Double* lagr = NULL; // output : lagrangian multipliers
    int lagr_size = 0;          //          and its size
    int info = 0; // will contain info from plcbas
    int iter = 0; // will contain num iter from plcbas

    /* function name */
    char * fname = "quapro_internal";

    /* check number of input arguments */
    if (in.size() != 9
     && in.size() != 10) {
        Scierror(77, _("%s: Wrong number of input arguments: "
                       "%d or %d expected.\n"),
                 fname, 9, 10);
        return types::Function::Error;
    }

    /* check number of output arguments */
    if (iRetCount != 2
     && iRetCount != 3) {
        Scierror(999, _("%s: Wrong number of output arguments: "
                        "%d or %d expected.\n"),
                 fname, 2, 3);
        return types::Function::Error;
    }

    /* Check that x0 is a vector of double */
    if ( in[0]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 1);
        return types::Function::Error;
    }
    o_x0 = in[0]->getAs<types::Double>();
    if ( o_x0->isVector() == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Vector expected."),
                 fname, 1);
        return types::Function::Error;
    }
    n_x0 = o_x0->getSize(); // get size of x0

    /* check that h is a square matrix of double */
    if ( in[1]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 2);
        return types::Function::Error;
    }
    o_h = in[1]->getAs<types::Double>();
    if ( o_h->getRows() != o_h->getCols() ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Square matrix expected.\n"),
                 fname, 2);
        return types::Function::Error;
    }
    n_h = o_h->getRows(); // get num rows of h == num cols of h

    /* check that p is a vector of double */
    if ( in[2]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 3);
        return types::Function::Error;
    }
    o_p = in[2]->getAs<types::Double>();
    if ( o_p->isVector() == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Vector expected."),
                 fname, 3);
        return types::Function::Error;
    }
    n_p = o_p->getSize(); // get size of p

    /* check that c is a matrix of double */
    if ( in[3]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 4);
        return types::Function::Error;
    }
    o_c = in[3]->getAs<types::Double>();
    r_c = o_c->getRows(); // get num rows of c
    c_c = o_c->getCols(); // get num cols of c

    /* check that d is a vector of double or [] */
    if ( in[4]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 5);
        return types::Function::Error;
    }
    o_d = in[4]->getAs<types::Double>();
    if ( o_d->isVector() == FALSE
     &&  o_d->isEmpty()  == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Vector or [] expected."),
                 fname, 5);
        return types::Function::Error;
    }
    n_d = o_d->getSize(); // get size of d

    /* check that ci is a vector of double or [] */
    if ( in[5]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 6);
        return types::Function::Error;
    }
    o_ci = in[5]->getAs<types::Double>();
    if ( o_ci->isVector() == FALSE
     &&  o_ci->isEmpty()  == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Vector or [] expected."),
                 fname, 6);
        return types::Function::Error;
    }
    n_ci = o_ci->getSize(); // get size of ci

    /* check that cs is a vector of double or [] */
    if ( in[6]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 7);
        return types::Function::Error;
    }
    o_cs = in[6]->getAs<types::Double>();
    if ( o_cs->isVector() == FALSE
     &&  o_cs->isEmpty()  == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Vector or [] expected."),
                 fname, 7);
        return types::Function::Error;
    }
    n_cs = o_cs->getSize(); // get size of cs

    /* check that mi is a scalar double */
    if ( in[7]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected."),
                 fname, 8);
        return types::Function::Error;
    }
    o_mi = in[7]->getAs<types::Double>();
    if ( o_mi->isScalar() == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Scalar expected."),
                 fname, 8);
        return types::Function::Error;
    }
    // get content of mi
    mi = static_cast<int>(o_mi->getReal(0, 0));

    /* check that modo is a scalar double in the set 1:3 */
    if ( in[8]->isDouble() == FALSE ) {
        Scierror(999, _("%s: Wrong type for argument #%d: "
                        "Matrix expected.\n"),
                 fname, 9);
        return types::Function::Error;
    }
    o_modo = in[8]->getAs<types::Double>();
    if ( o_modo->isScalar() == FALSE ) {
        Scierror(999, _("%s: Wrong size for argument #%d: "
                        "Scalar expected."),
                 fname, 9);
        return types::Function::Error;
    }
    // get content of variable modo
    modo = static_cast<int>(o_modo->getReal(0, 0));
    if ( modo != 1
     &&  modo != 2
     &&  modo != 3 ) {
        Scierror(999, _("%s: Wrong input argument #%d: "
                        "1, 2 or 3 expected.\n"), // this won't be localized
                                                  // but it's more explicit
                 fname, 9);
        return types::Function::Error;
    }

    /* is imp available ? */
    if ( in.size() == 10 ) {
        /* check that imp is a scalar double */
        if ( in[9]->isDouble() == FALSE ) {
            Scierror(999, _("%s: Wrong type for argument #%d: "
                            "Matrix expected.\n"),
                     fname, 10);
            return types::Function::Error;
        }
        o_imp = in[9]->getAs<types::Double>();
        if ( o_imp->isScalar() == FALSE ) {
            Scierror(999, _("%s: Wrong size for argument #%d: "
                            "Scalar expected.\n"),
                     fname, 10);
            return types::Function::Error;
        }
        // get content of variable imp
        imp = static_cast<int>(o_imp->getReal(0, 0)) ;
    } else { /* if not, default to 0.0 */
        imp = 0.0;
    }

    /* cross variable size checking */
    if (n_p != n_h) {
        Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                        "matrix h and vector p must have "
                        "the same dimension.\n"),
                 fname, 2, 3);
        return types::Function::Error;
    }
    if (n_x0 != n_p) {
        if (modo == 3) {
            Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                            "vector x0 and vector p must have "
                            "the same dimension.\n"),
                     fname, 1, 3);
            return types::Function::Error;
        } else {
            n_x0 = n_p;
        }
    }
    if (r_c != 0 && n_p != r_c) {
        Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                        "if matrix c is not [], it must have "
                        "the same dimension as the vector p.\n"),
                 fname, 3, 4);
        return types::Function::Error;
    }
    if (n_ci == 0) {
        ira = 0;
    } else if (n_x0 != n_ci) {
        Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                        "if ci is not [], it must have "
                        "the same dimension as the vector x0.\n"),
                 fname, 1, 6);
        return types::Function::Error;
    } else {
        ira = 1;
    }
    if (n_cs == 0) {
        // ira = ira + 0
    } else if (n_x0 != n_cs) {
        Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                        "if cs is not [], it must have "
                        "the same dimension as the vector x0.\n"),
                 fname, 1, 7);
        return types::Function::Error;
    } else {
        ira += 2;
    }
    if (c_c != n_d) {
        Scierror(999, _("%s: Incompatible input arguments #%d and #%d: "
                        "matrix c must have the same num cols "
                        "as the size of vector d.\n"),
                 fname, 4, 5);
        return types::Function::Error;
    }
    md = c_c - mi; // TODO what is this

    x0_cont = o_x0->get();
    // prepare x, with x0 as initial value (this will be out[1])
    x = new types::Double(o_x0->getRows(), o_x0->getCols(), &x0_cont);

    // prepare empty f (this will be out[2])
    f = new types::Double(1, 1);

    // workspace vector (see plcbas.f - W info)
    w  = new double[n_x0*n_x0 + 6*n_x0 + 2*md];

    // workspace vector (see plcbas.f - IV info)
    iv = new int[3*n_x0 + 2*md + mi + 1];

    // prepare lagr (this will be out[3]) (see plcbas.f - DLAGR info)
    if (ira > 0) {
        lagr_size = n_x0 + r_c - 1;
    } else {
        lagr_size = r_c - 1;
    }
    lagr = new types::Double(lagr_size, 1);

    iter = 0;

    wte = 6; // stdout id in fortran

    //========================//
    //                        //
    //     CALL TO PLCBAS     //
    //                        //
    //========================//
    C2F(plcbas)(
            o_h->get(),  // stk(l2)
            o_p->get(),  // stk(l3)
            o_c->get(),  // stk(l4)
            o_d->get(),  // stk(l5)
            o_ci->get(), // stk(l6)
            o_cs->get(), // stk(l7)
            &ira, &mi, &md,
            x->get(),    // stk(lw9)
            f->get(),    // stk(lw10)
            w, iv,
            lagr->get(), // stk(lw13)
            &imp, &wte, &n_x0, &modo,
            &info, &iter);

    // free workspace vectors
    delete[] w;
    delete[] iv;

    // deal with info (return state)
    switch (info) {
    case 1 :
        Scierror(999, _("%s: Variable %s is not a valid "
                        "state space representation.\n"),
                 fname, 0);
        goto plcbas_error;
    case -1 :
        Scierror(999, _("%s: Function not bounded from below.\n"),
                 fname);
        goto plcbas_error;
    case -2 :
        Scierror(999, _("%s: Inconsistent right division.\n"),
                 fname);
        goto plcbas_error;
    case -3 :
        Scierror(999, _("%s: Problem may be unbounded: "
                        "too high distance between two "
                        "consecutive iterations.\n"),
                 fname);
        goto plcbas_error;
    case -4 :
        Scierror(999, _("%s: Incompatible input argument.\n"),
                 fname);
        goto plcbas_error;
    case -11 :
        Scierror(999, _("%s: Inconsistent constraints.\n"),
                 fname);
        goto plcbas_error;
    case -12 :
        Scierror(999, _("%s: No feasible solution.\n"),
                 fname);
        goto plcbas_error;
    case -13 :
        Scierror(999, _("%s: Degenerate starting point.\n"),
                 fname);
        goto plcbas_error;
    case -14 :
        Scierror(999, _("%s: No feasible point has been found.\n"),
                 fname);
        goto plcbas_error;
    }

    // propagate x, f and (optionally) lagr to output

    out.push_back(x);

    out.push_back(f);

    if (iRetCount >= 3) {
        out.push_back(lagr);
    }

    return types::Function::OK;

plcbas_error:
    delete[] x;
    delete[] f;
    delete[] lagr;
    return types::Function::Error;
}
