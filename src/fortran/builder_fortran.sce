// ====================================================================
// Copyright (C) INRIA -  Serge Steer, Allan CORNET
// Copyright (C) DIGITEO - 2010 -  Allan CORNET
// Copyright (C) DIGITEO - 2011 -  Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

src_dir = get_absolute_file_path("builder_fortran.sce");

linknames = "pclbas";
files=[
"aind.f"
"anfm01.f"
"anfm02.f"
"anfm03.f"
"anfm04.f" 
"anfm05.f"
"anfm06.f"
"anrs01.f"
"anrs02.f"
"aux003.f"
"auxo01.f"
"desr03.f"
"dimp03.f"
"dipvtf.f"
"dnrm0.f" 
"nvkt03.f"
"optr01.f"
"optr03.f"
"opvf03.f"
"pasr03.f"
"plcbas.f"
"tol03.f"
"zthz.f"
"dadd.f"
"ddif.f"
"dmmul.f"
];
src_path = "f";
libs = [];
ldflags = "";

tbx_build_src(linknames, files, src_path, src_dir, libs, ldflags);

clear tbx_build_src;
