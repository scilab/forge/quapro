// =============================================================================
// Copyright (C) INRIA -  Serge Steer, Allan CORNET
// Copyright (C) DIGITEO - 2010 -  Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
mode(-1);
lines(0);

TOOLBOX_NAME = "quapro";
TOOLBOX_TITLE = "quapro";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab"s version
// =============================================================================
try
    v = getversion("scilab");
catch
    error(gettext("Scilab 6.0 or more is required."));
end

if v(1) < 6 then
    error(gettext("Scilab 6.0 or more is required."));
end
clear v;

// Action
// =============================================================================
tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Clean variables
// =============================================================================
clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;

